<?php

namespace TheFeed\Service;

use PhpParser\Node\Expr\Array_;
use TheFeed\Controleur\ControleurPublication;
use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use Symfony\Component\HttpFoundation\Response;

class PublicationService implements PublicationServiceInterface
{
    private static PublicationRepositoryInterface $pub;
    private static UtilisateurRepositoryInterface $uti;

    public function __construct(PublicationRepositoryInterface $pub,UtilisateurRepositoryInterface $uti)
    {
        self::$pub = $pub;
        self::$uti = $uti;
    }


    /**
     * @throws \Exception
     */
    public function recupererPublications(): array{
        return self::$pub->recuperer();
    }

    /**
     * @throws ServiceException
     */
    public function creerPublication($idUtilisateur, $message): void
    {
        $utilisateur = self::$uti->recupererParClePrimaire($idUtilisateur);

        if ($utilisateur == null) {
            throw new ServiceException( "Il faut être connecté pour publier un feed");
        }

        if ($message == null || $message == "") {
            throw new ServiceException( "Le message ne peut pas être vide!");
        }
        if (strlen($message) > 250) {
            throw new ServiceException("Le message ne peut pas dépasser 250 caractères!");
        }

        $publication = Publication::create($message, $utilisateur);
        self::$pub->ajouter($publication);
    }

    /**
     * @throws ServiceException
     */
    public function recupererPublicationsUtilisateur($idUtilisateur): array
    {
        if ($idUtilisateur === null) {
            throw new ServiceException( "Utilisateur inexistant.");
        } else {
           return self::$pub->recupererParAuteur($idUtilisateur);
        }
    }
    public function supprimerPublication(int $idPublication, ?string $idUtilisateurConnecte): void
    {
        $publication = self::$pub->recupererParClePrimaire($idPublication);

        if (is_null($idUtilisateurConnecte))
            throw new ServiceException("Il faut être connecté pour supprimer une publication", Response::HTTP_FORBIDDEN);

        if ($publication === null)
            throw new ServiceException("Publication inconnue.", Response::HTTP_NOT_FOUND);

        if ($publication->getAuteur()->getIdUtilisateur() !== intval($idUtilisateurConnecte))
            throw new ServiceException("Seul l'auteur de la publication peut la supprimer", Response::HTTP_UNAUTHORIZED);

        self::$pub->supprimer($publication);
    }


}