<?php

namespace TheFeed\Service;

use TheFeed\Controleur\ControleurUtilisateur;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface
{
    private static UtilisateurRepositoryInterface $uti;
    private static string $dossierPhotoDeProfil;
    private static FileMovingServiceInterface $fileMovingService;

    public function __construct(UtilisateurRepositoryInterface $uti, string $dossierPhotoDeProfil, FileMovingServiceInterface $fileMovingService)
    {
        self::$uti = $uti;
        self::$dossierPhotoDeProfil = $dossierPhotoDeProfil;
        self::$fileMovingService = $fileMovingService;
    }
    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil): void
    {
        if (
            isset($login) && isset($motDePasse) && isset($adresseMail)
            && isset($nomPhotoDeProfil)
        ) {
            if (strlen($login) < 4 || strlen($login) > 20) {
                throw new ServiceException( "Le login doit être compris entre 4 et 20 caractères!");
            }
            if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                throw new ServiceException( "Mot de passe invalide!");
            }
            if (!filter_var($adresseMail, FILTER_VALIDATE_EMAIL)) {
                throw new ServiceException( "L'adresse mail est incorrecte!");
            }

            $utilisateurRepository = self::$uti;
            $utilisateur = $utilisateurRepository->recupererParLogin($login);
            if ($utilisateur != null) {
                throw new ServiceException("Ce login est déjà pris!" );
            }

            $utilisateur = $utilisateurRepository->recupererParEmail($adresseMail);
            if ($utilisateur != null) {
                throw new ServiceException( "Un compte est déjà enregistré avec cette adresse mail!");
            }

            $mdpHache = MotDePasse::hacher($motDePasse);

            // Upload des photos de profil
            // Plus d'informations :
            // http://romainlebreton.github.io/R3.01-DeveloppementWeb/assets/tut4-complement.html

            // On récupère l'extension du fichier
            $explosion = explode('.', $nomPhotoDeProfil['name']);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                throw new ServiceException( "La photo de profil n'est pas au bon format!");
            }
            // La photo de profil sera enregistrée avec un nom de fichier aléatoire
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $nomPhotoDeProfil['tmp_name'];
            ///../../ressources/img/utilisateurs
            $to = self::$dossierPhotoDeProfil."/$pictureName";
            self::$fileMovingService->moveFile($from, $to);

            $utilisateur = Utilisateur::create($login, $mdpHache, $adresseMail, $pictureName);
            $utilisateurRepository->ajouter($utilisateur);
        } else {
            throw new ServiceException( "Login, nom, prenom ou mot de passe manquant.");
        }
    }

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur {
        $utilisateur = self::$uti->recupererParClePrimaire($idUtilisateur);
        if(!$autoriserNull && !isset($utilisateur)) {
            throw new ServiceException( "Login, nom, prenom ou mot de passe manquant.");
        }
     return $utilisateur;
    }

    /**
     * @throws ServiceException
     */
    public function connecter($login, $mdp): void
    {
        if (!(isset($login) && isset($mdp))) {
            throw new ServiceException( "Login ou mot de passe manquant.");
        }
        $utilisateurRepository = self::$uti;
        /** @var Utilisateur $utilisateur */
        $utilisateur = $utilisateurRepository->recupererParLogin($login);
        if ($utilisateur == null) {
            throw new ServiceException( "Login inconnu.");
        }

        if (!MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
            throw new ServiceException("Mot de passe incorrect.");
        }
        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
    }

    /**
     * @throws ServiceException
     */
    public function deconnecter(){
        if (!ConnexionUtilisateur::estConnecte()) {
            throw new ServiceException("Utilisateur non connecté.");
        }
        ConnexionUtilisateur::deconnecter();
    }
}