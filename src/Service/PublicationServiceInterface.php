<?php

namespace TheFeed\Service;

use TheFeed\Service\Exception\ServiceException;

interface PublicationServiceInterface
{
    /**
     * @throws \Exception
     */
    public function recupererPublications(): array;

    /**
     * @throws ServiceException
     */
    public function creerPublication($idUtilisateur, $message): void;

    /**
     * @throws ServiceException
     */
    public function recupererPublicationsUtilisateur($idUtilisateur): array;

    public function supprimerPublication(int $idPublication, ?string $idUtilisateurConnecte): void;
}