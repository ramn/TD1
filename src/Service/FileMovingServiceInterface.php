<?php
namespace TheFeed\Service;

interface FileMovingServiceInterface
{
    public function moveFile($fileName, $pathDestination);
}
