<?php

namespace TheFeed\Service;

use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Service\Exception\ServiceException;

interface UtilisateurServiceInterface
{
    /**
     * @throws ServiceException
     */
    public function creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil): void;

    /**
     * @throws ServiceException
     */
    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur;

    /**
     * @throws ServiceException
     */
    public function connecter($login, $mdp): void;

    /**
     * @throws ServiceException
     */
    public function deconnecter();
}