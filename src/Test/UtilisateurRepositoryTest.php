<?php
namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\ConnexionBaseDeDonneesI;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;

class UtilisateurRepositoryTest extends TestCase
{
    private static UtilisateurRepositoryInterface  $utilisateurRepository;

    private static ConnexionBaseDeDonneesI $connexionBaseDeDonnees;

    public static function setUpBeforeClass(): void
    {
        parent::setUpBeforeClass();
        self::$connexionBaseDeDonnees = new ConnexionBaseDeDonnees(new ConfigurationBDDTestUnitaire());
        self::$utilisateurRepository = new UtilisateurRepository(self::$connexionBaseDeDonnees);
    }

    protected function setUp(): void
    {
        parent::setUp();
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur,login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (1,'test', 'test', 'test@example.com', 'test.png')");
        self::$connexionBaseDeDonnees->getPdo()->query("INSERT INTO 
                                                         utilisateurs (idUtilisateur, login, mdpHache, email, nomPhotoDeProfil) 
                                                         VALUES (2,'test2', 'test2', 'test2@example.com', 'test2.png')");
    }

    public function testSimpleNombreUtilisateurs() {
        $this->assertCount(2, self::$utilisateurRepository->recuperer());
    }
    public function testRecupParId() {
        $utu = self::$utilisateurRepository->recupererParClePrimaire(1);
        $this->assertEquals("test",$utu->getLogin());
    }
    public function testRecupParLogin() {
        $this->assertEquals(1, self::$utilisateurRepository->recupererParLogin("test")->getIdUtilisateur());
    }

    public function testRecupParEmail() {
        $this->assertEquals("test", self::$utilisateurRepository->recupererParEmail("test@example.com")->getLogin());
    }

    public function testAjouter() {
        $utu = Utilisateur::create("test6","tsdy","test6@exemple.com","test2.png");
        $id = self::$utilisateurRepository->ajouter($utu);
        $this->assertEquals("test6", self::$utilisateurRepository->recupererParClePrimaire($id)->getLogin());
    }
    public function testMettreAjour() {
        $utu = Utilisateur::create("test4","","test4@exemple.com","test1.png");
        $utu->setIdUtilisateur(1);
        self::$utilisateurRepository->mettreAJour($utu);
        $this->assertEquals("test4", self::$utilisateurRepository->recupererParClePrimaire(1)->getLogin());
    }
    public function testSupp() {
        $values = [
            "idUtilisateur" => "1",
        ];
        $utu = self::$utilisateurRepository->recupererParClePrimaire(1);
        self::$utilisateurRepository->supprimer($utu);
        $this->assertNull(self::$utilisateurRepository->recupererParClePrimaire(1));
    }
    protected function tearDown(): void
    {
        parent::tearDown();
        self::$connexionBaseDeDonnees->getPdo()->query("DELETE FROM utilisateurs");
    }

}