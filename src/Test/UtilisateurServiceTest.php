<?php
namespace TheFeed\Test;

use PHPUnit\Framework\TestCase;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Modele\Repository\UtilisateurRepositoryInterface;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\FileMovingServiceInterface;
use TheFeed\Service\UtilisateurService;
use function PHPUnit\Framework\assertFalse;

class UtilisateurServiceTest extends TestCase
{

    private $service;

    private $utilisateurRepositoryMock;

    //Dossier où seront déplacés les fichiers pendant les tests
    private  $dossierPhotoDeProfil = __DIR__."/tmp/";

    private FileMovingServiceInterface $fileMovingService;

    protected function setUp(): void
    {
        parent::setUp();
        $this->utilisateurRepositoryMock = $this->createMock(UtilisateurRepository::class);
        $this->fileMovingService = new TestFileMovingService();
        mkdir($this->dossierPhotoDeProfil);
        $this->service = new UtilisateurService($this->utilisateurRepositoryMock,$this->dossierPhotoDeProfil,$this->fileMovingService);
    }

    public function testCreerUtilisateurPhotoDeProfil() {
        $donneesPhotoDeProfil = [];
        $donneesPhotoDeProfil["name"] = "test.png";
        $donneesPhotoDeProfil["tmp_name"] = "test.png";
        $this->utilisateurRepositoryMock->method("recupererParLogin")->willReturn(null);
        $this->utilisateurRepositoryMock->method("recupererParEmail")->willReturn(null);
        $this->utilisateurRepositoryMock->method("ajouter")->willReturnCallback(function ($utilisateur) {
            /* TODO : Tester l'existence du fichier (et eventuellement d'autres tests) */
            self::assertFileExists($this->dossierPhotoDeProfil.$utilisateur->getNomPhotoDeProfil());
        });
        $this->service->creerUtilisateur("test", "TestMdp123", "test@example.com", $donneesPhotoDeProfil);
    }

    protected function tearDown(): void
    {
        //Nettoyage
        parent::tearDown();
        foreach(scandir($this->dossierPhotoDeProfil) as $file) {
            if ('.' === $file || '..' === $file) continue;
            unlink($this->dossierPhotoDeProfil.$file);
        }
        rmdir($this->dossierPhotoDeProfil);
    }
    public function testRecupererParId() {
       $utu  = Utilisateur::create("test", "TestMdp123", "test@example.com", "donneesPhotoDeProfil");
        $this->utilisateurRepositoryMock->method("recupererParId")->withAnyParameters()->willReturn($utu);
        self::assertEquals("test",$this->service->recupererUtilisateurParId(1)->getLogin());
    }
    public function testConnection(){
        $utu  = Utilisateur::create("test", MotDePasse::hacher("TestMdp123"), "test@example.com", "donneesPhotoDeProfil");
        $utu->setIdUtilisateur(1);
        $this->utilisateurRepositoryMock->method("recupererParLogin")->withAnyParameters()->willReturn($utu);
        $this->service->connecter(1,"TestMdp123");
        self::assertEquals(1,ConnexionUtilisateur::getIdUtilisateurConnecte());

    }
    public function testLoginInconnu(){
        $this->expectException(ServiceException::class);
        $utu  = Utilisateur::create("test", MotDePasse::hacher("TestMdp123"), "test@example.com", "donneesPhotoDeProfil");
        $utu->setIdUtilisateur(1);
        $this->utilisateurRepositoryMock->method("recupererParLogin")->withAnyParameters()->willReturn(null);
        $this->expectExceptionMessage("Login inconnu.");
        $this->service->connecter(1,"TestMdp123");
    }
    public function testDeco(){
        $utu  = Utilisateur::create("test", MotDePasse::hacher("TestMdp123"), "test@example.com", "donneesPhotoDeProfil");
        $utu->setIdUtilisateur(1);
        $this->utilisateurRepositoryMock->method("recupererParLogin")->withAnyParameters()->willReturn($utu);
        $this->service->connecter(1,"TestMdp123");
        $this->service->deconnecter();
        assertFalse(ConnexionUtilisateur::estConnecte());
    }

}
