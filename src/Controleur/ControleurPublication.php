<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\PublicationRepositoryInterface;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ControleurPublication extends ControleurGenerique
{

    private  PublicationServiceInterface $pu;

    public function __construct(ContainerInterface $container, private PublicationServiceInterface $publicationService)
    {
        parent::__construct($container);
        $this->pu = $this->publicationService;
    }

    /**
     * @throws \Exception
     */
    #[Route(path: '/publications', name:'afficherListe', methods:["GET"])]
    #[Route(path: '/', name:'Liste',)]
    public  function afficherListe() : Response
    {
        return $this->afficherTwig('publication/feed.html.twig', [
             "publications" => $this->pu->recupererPublications(),
         ]);
    }
    #[Route(path: '/publications', name:'CreeListe', methods:["POST"])]
    public  function creerDepuisFormulaire() : RedirectResponse
    {
        $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
        $message = $_POST['message'];
        try {
            $this->pu->creerPublication($idUtilisateurConnecte,$message);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger('afficherListe');
        }
        return $this->rediriger('afficherListe');
    }


}