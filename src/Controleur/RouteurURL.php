<?php

namespace TheFeed\Controleur;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ContainerControllerResolver;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\Conteneur;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\MessageFlash;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\AttributeDirectoryLoader;
use TheFeed\Lib\AttributeRouteControllerLoader;
use TheFeed\Modele\Repository\ConnexionBaseDeDonnees;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\PublicationService;
use TheFeed\Service\UtilisateurService;
use TheFeed\Configuration\ConfigurationBDDMySQL;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
class RouteurURL
{
    /**
     * @throws \Exception
     */
    public static function traiterRequete(Request $requete) : Response
    {
        $conteneur = new ContainerBuilder();
        $conteneur->setParameter('project_root', __DIR__.'/../..');



       /* $conteneur->register('configuration_bdd_my_sql', ConfigurationBDDMySQL::class);

        $connexionBaseService = $conteneur->register('connexion_base_de_donnees', ConnexionBaseDeDonnees::class);
        $connexionBaseService->setArguments([new Reference('configuration_bdd_my_sql')]);

        $publicationsRepositoryService = $conteneur->register('publication_repository',PublicationRepository::class);
        $publicationsRepositoryService->setArguments([new Reference('connexion_base_de_donnees')]);

        $utilisateurRepositoryService = $conteneur->register('utilisateur_repository',UtilisateurRepository::class);
        $utilisateurRepositoryService->setArguments([new Reference('connexion_base_de_donnees')]);

        $publicationService = $conteneur->register('publication_service', PublicationService::class);
        $publicationService->setArguments([new Reference('publication_repository'), new Reference('utilisateur_repository')]);

        $utilisateurService = $conteneur->register('utilisateur_service', UtilisateurService::class);
        $utilisateurService->setArguments([new Reference('utilisateur_repository')]);

        $publicationControleurService = $conteneur->register('controleur_publication',ControleurPublication::class);
        $publicationControleurService->setArguments([new Reference('publication_service')]);

        $utilisateurControleurService = $conteneur->register('controleur_utilisateur',ControleurUtilisateur::class);
        $utilisateurControleurService->setArguments([new Reference('publication_service'), new Reference('utilisateur_service')]);
*/

        $loader = new YamlFileLoader($conteneur, new FileLocator(__DIR__."/../Configuration"));
        //On remplit le conteneur avec les données fournies dans le fichier de configuration
        $loader->load("conteneur.yml");

        $routes = self::getRoutesCollection();

        $conteneur->set('container', $conteneur);



        //var_dump($requete->getPathInfo());

        $contexteRequete = (new RequestContext())->fromRequest($requete);

        //var_dump($contexteRequete);
        $conteneur->set('request_context', (new RequestContext())->fromRequest($requete));

        //Après que les routes soient récupérées
        $conteneur->set('routes', $routes);

       /* $generateurUrl = new UrlGenerator($routes, $contexteRequete);
        $assistantUrl = new UrlHelper(new RequestStack(), $contexteRequete);*/
        $generateurUrl = $conteneur->get('url_generator');
        $assistantUrl = $conteneur->get('url_helper');
        $twigLoader = new FilesystemLoader(__DIR__ . '/../vue/');

        $twig=$conteneur->get('twig');

        $twig->addFunction(new \Twig\TwigFunction('estConnecte', function () {
            return ConnexionUtilisateur::estConnecte();
        }));
        $twig->addFunction(new \Twig\TwigFunction('idU', function () {
            return  ConnexionUtilisateur::getIdUtilisateurConnecte();
        }));

        $twig->addGlobal('messagesFlash', new MessageFlash());

        //$callable = [$generateurUrl, "generate"];
        $twig->addFunction(new TwigFunction("route", $generateurUrl->generate(...)));
        $twig->addFunction(new TwigFunction("asset", $assistantUrl->getAbsoluteUrl(...)));
/*
        Conteneur::ajouterService("generateurUrl", $generateurUrl);
        Conteneur::ajouterService("assistantUrl", $assistantUrl);
*/
        //Après l'instanciation de l'objet $request

        try {
            $associateurUrl = new UrlMatcher($routes, $contexteRequete);
            $donneesRoute = $associateurUrl->match($requete->getPathInfo());
            $requete->attributes->add($donneesRoute);

            $resolveurDeControleur = new ContainerControllerResolver($conteneur);
            $controleur = $resolveurDeControleur->getController($requete);

            $resolveurDArguments = new ArgumentResolver();
            $arguments = $resolveurDArguments->getArguments($requete, $controleur);

            $reponse = call_user_func_array($controleur, $arguments);
        } catch (MethodNotAllowedException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = ($conteneur->get('ControleurGenerique'))->afficherErreur($exception->getMessage(), 405);
        } catch (ResourceNotFoundException $exception) {
            // Remplacez xxx par le bon code d'erreur
            $reponse = ($conteneur->get('ControleurGenerique'))->afficherErreur($exception->getMessage(), 404);
        } catch (\Exception $exception) {
            $reponse = ($conteneur->get('ControleurGenerique'))->afficherErreur($exception->getMessage());
        }
        return $reponse;
    }

    public static function getRoutesCollection(): RouteCollection
    {
      /*  $routes = new RouteCollection();

        $route = new Route("/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("afficherListe", $route);

        $route = new Route("/utilisateurs/{idUtilisateur}/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherPublications",
        ]);
        $route->setMethods(["GET"]);
        $routes->add("afficherPublications", $route);

        $route = new Route("/", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::afficherListe",
        ]);
        $routes->add("Liste", $route);

        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireConnexion",
            // Syntaxes équivalentes
            // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
            // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],
        ]);
        $route->setMethods(["GET"]);
        $routes->add("afficherFormulaireConnexion", $route);

        $route = new Route("/connexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::connecter",
            // Syntaxes équivalentes
            // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
            // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],
        ]);
        $route->setMethods(["POST"]);
        $routes->add("connexion", $route);

        $route = new Route("/deconnexion", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::deconnecter",
            // Syntaxes équivalentes
            // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
            // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],
        ]);
        $route->setMethods(["GET"]);
        $routes->add("deconection", $route);

        $route = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::afficherFormulaireCreation",
            // Syntaxes équivalentes
            // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
            // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],
        ]);
        $route->setMethods(["GET"]);
        $routes->add("inscription", $route);

        $route = new Route("/inscription", [
            "_controller" => "\TheFeed\Controleur\ControleurUtilisateur::creerDepuisFormulaire",
            // Syntaxes équivalentes
            // "_controller" => ControleurUtilisateur::class . "::afficherFormulaireConnexion",
            // "_controller" => [ControleurUtilisateur::class, "afficherFormulaireConnexion"],
        ]);
        $route->setMethods(["POST"]);
        $routes->add("inscrire", $route);

        $route = new Route("/publications", [
            "_controller" => "\TheFeed\Controleur\ControleurPublication::creerDepuisFormulaire",
        ]);
        $route->setMethods(["POST"]);
        $routes->add("CreeListe", $route);

        return $routes;*/
        $fileLocator = new FileLocator(__DIR__);
        $attrClassLoader = new AttributeRouteControllerLoader();
        return (new AttributeDirectoryLoader($fileLocator, $attrClassLoader))->load(__DIR__);
    }
}