<?php

namespace TheFeed\Controleur;

use http\Env\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Response as Res;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\UtilisateurService;
use TheFeed\Service\UtilisateurServiceInterface;

class ControleurUtilisateur extends ControleurGenerique
{
    private static PublicationServiceInterface $pu;
    private static UtilisateurServiceInterface $ut;

    public function __construct(ContainerInterface $container, PublicationServiceInterface $pu, UtilisateurServiceInterface $ut)
    {
        self::$pu =$pu;
        self::$ut =$ut;
        parent::__construct($container);
    }

    public function afficherErreur($messageErreur = "", $controleur = ""): Res
    {
        return $this->afficherErreur($messageErreur, "utilisateur");
    }
    #[Route(path: '/utilisateurs/{idUtilisateur}/publications', name:'afficherPublications', methods:["GET"])]
    public  function afficherPublications($idUtilisateur): Res
    {
        try {
            $utilisateur = self::$ut->recupererUtilisateurParId($idUtilisateur,false);
            $loginHTML = htmlspecialchars($utilisateur->getLogin());
            $publications = self::$pu->recupererPublicationsUtilisateur($idUtilisateur);
            return $this->afficherTwig('publication/page_perso.html.twig', [
                "publications" => $publications,
                "pagetitle" => "Page de $loginHTML",
            ]);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger('afficherListe');
        }
    }
    #[Route(path: '/inscription', name:'inscription', methods:["GET"])]
    public  function afficherFormulaireCreation(): Res
    {
        $res = $this->afficherTwig('utilisateur/inscription.html.twig', [
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
        return $res;
    }
    #[Route(path: '/inscription', name:'inscrire', methods:["POST"])]
    public  function creerDepuisFormulaire(): RedirectResponse
    {
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;
        $adresseMail = $_POST['email'] ?? null;
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;
        try {
            self::$ut->creerUtilisateur($login,$motDePasse,$adresseMail,$nomPhotoDeProfil);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger('inscription');
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
        return $this->rediriger("afficherListe");
    }

    #[Route(path: '/connexion', name:'afficherFormulaireConnexion', methods:["GET"])]
    public  function afficherFormulaireConnexion(): Res
    {
        $res = $this->afficherTwig('utilisateur/connexion.html.twig', [
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
        return $res;
    }
    #[Route(path: '/connexion', name:'connexion', methods:["POST"])]
    public  function connecter(): RedirectResponse
    {
        try {
            $login = $_POST['login'] ?? null;
            $mdp = $_POST['mot-de-passe'] ?? null;
            self::$ut->connecter($login,$mdp);
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger('afficherFormulaireConnexion');
        }
        MessageFlash::ajouter("success", "Connexion effectuée.");
        return $this->rediriger("afficherListe");
    }
    #[Route(path: '/deconnexion', name:'deconection', methods:["GET"])]
    public  function deconnecter(): RedirectResponse
    {
        try {
            self::$ut->deconnecter();
        }
        catch(ServiceException $e) {
            MessageFlash::ajouter("error", $e->getMessage());
            return $this->rediriger('afficherListe');
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return $this->rediriger( "afficherListe");
    }
}
