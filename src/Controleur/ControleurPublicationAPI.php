<?php
namespace TheFeed\Controleur;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Annotation\Route;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Service\PublicationServiceInterface;
use TheFeed\Service\Exception\ServiceException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ControleurPublicationAPI extends ControleurGenerique
{

    public function __construct (
        ContainerInterface $container,
        private readonly PublicationServiceInterface $publicationService
    )
    {
        parent::__construct($container);
    }
    #[Route(path: '/api/publications/{idPublication}', name:'supprimerPublicationsAPI', methods:["DELETE"])]
    public function supprimer($idPublication): Response
    {
        try {
            $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
            $this->publicationService->supprimerPublication($idPublication, $idUtilisateurConnecte);
            return new JsonResponse('', Response::HTTP_OK);
        } catch (ServiceException $exception) {
            return new JsonResponse(["error" => $exception->getMessage()], $exception->getCode());
        }
    }
}
